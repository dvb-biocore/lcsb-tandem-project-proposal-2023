# LCSB Tandem Project Proposal 2023

## Supplementary file 1

Example of high-content imaging data output. Columns A to H represent metadata, which includes relevant information about each sample. 
Columns I to AA represent different features extracted from the imaging experiment. 

[Supplementary file 1](https://gitlab.lcsb.uni.lu/dvb-biocore/lcsb-tandem-project-proposal-2023/-/blob/main/data/Supplementary%20file%201.csv)
